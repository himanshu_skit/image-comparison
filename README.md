# image-comparison
Compare Visual appearance of images

## Structure

Move to the base of the image-comparison  i.e, `${PROJECT_CHECKOUT_FOLDER}\image-comparison`

- `src/main/java` for all java classes.
- `src/main/resources` having csv files.

## Build

A prerequisite to build the project is `gradle`.

    cd ${PROJECT_CHECKOUT_FOLDER}\image-comparison
    ./gradlew clean build # build 
    ./gradlew run # Run the Image comparison application 
A
[gradle plugin](https://docs.gradle.org/current/userguide/eclipse_plugin.html)
is used to prepare the project for eclipse import. Run:

- Run `./gradlew eclipse`
- Import project into eclipse

