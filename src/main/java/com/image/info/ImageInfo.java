package com.image.info;

public class ImageInfo {

    private String image1Path;
    private String image2Path;
    private Double score;
    private Double elapsedTime;

    public ImageInfo(String image1Path, String image2Path, Double score, Double elapsedTime) {
        this.image1Path = image1Path;
        this.image2Path = image2Path;
        this.score = score;
        this.elapsedTime = elapsedTime;
    }



    public ImageInfo(String image1Path, String image2Path) {
        this.image1Path = image1Path;
        this.image2Path = image2Path;
    }

    public ImageInfo() {

    }

    public String getImage1Path() {
        return image1Path;
    }

    public String getImage2Path() {
        return image2Path;
    }

    public Double getScore() {
        return score;
    }

    public Double getElapsedTime() {
        return elapsedTime;
    }

    public void setImage1Path(String image1Path) {
        this.image1Path = image1Path;
    }

    public void setImage2Path(String image2Path) {
        this.image2Path = image2Path;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public void setElapsedTime(Double elapsedTime) {
        this.elapsedTime = elapsedTime;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ImageInfo{");
        sb.append("image1Path='").append(image1Path).append('\'');
        sb.append(", image2Path='").append(image2Path).append('\'');
        sb.append(", score='").append(score).append('\'');
        sb.append(", elapsedTime='").append(elapsedTime).append('\'');
        sb.append('}');
        return sb.toString();
    }


}
