package com.image.exception;

public class ImageComparisonException extends RuntimeException {
    private String exceptionMsg;

    public ImageComparisonException(String message) {
        this.exceptionMsg = message;
    }
}
