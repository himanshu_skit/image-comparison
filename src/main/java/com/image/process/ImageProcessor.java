package com.image.process;

import com.image.info.ImageInfo;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;

public class ImageProcessor {
    public void process(ImageInfo imageInfo) throws IOException {


        BufferedImage image1 = getBufferedImage(imageInfo.getImage1Path());
        BufferedImage image2 = getBufferedImage(imageInfo.getImage2Path());

        Instant start = Instant.now();
        double score = calculateScore(image1, image2);
        Instant end = Instant.now();
        Duration timeElapsed = Duration.between(start, end);

        double elapsedTimeInSecond = (double) timeElapsed.toNanos() / 1_000_000_000;
        imageInfo.setScore(score);
        imageInfo.setElapsedTime((double) elapsedTimeInSecond);
    }

    private double calculateScore(BufferedImage imageA, BufferedImage imageB) {
        double score = 0;
        if(!isDimensionEqual(imageA,imageB)){
            score = -1;
            return score;
        }

        int width1 = imageA.getWidth();
        int width2 = imageB.getWidth();
        int height1 = imageA.getHeight();
        int height2 = imageB.getHeight();

        long difference = 0;
        for (int y = 0; y < height1; y++)
        {
            for (int x = 0; x < width1; x++)
            {
                int rgbA = imageA.getRGB(x, y);
                int rgbB = imageB.getRGB(x, y);

                int redA = (rgbA >> 16) & 0xff;
                int greenA = (rgbA >> 8) & 0xff;
                int blueA = (rgbA) & 0xff;

                int redB = (rgbB >> 16) & 0xff;
                int greenB = (rgbB >> 8) & 0xff;
                int blueB = (rgbB) & 0xff;

                difference += Math.abs(redA - redB);
                difference += Math.abs(greenA - greenB);
                difference += Math.abs(blueA - blueB);
            }
        }

         double total_pixels = width1 * height1 * 3;

        // Normalizing the value of different pixels
        // for accuracy(average pixels per color
        // component)
        double avg_different_pixels = difference / total_pixels;

        // There are 255 values of pixels in total
        score  = (avg_different_pixels / 255) ;

        return score;
    }

    private boolean isDimensionEqual(BufferedImage bufferedImage1, BufferedImage bufferedImage2) {
        int width1 = bufferedImage1.getWidth();
        int width2 = bufferedImage2.getWidth();
        int height1 = bufferedImage1.getHeight();
        int height2 = bufferedImage2.getHeight();

        return ((width1 == width2) && (height1 == height2));

    }

    private BufferedImage getBufferedImage(String path) throws IOException {
        File file = new File(path);
        BufferedImage image = ImageIO.read(file);
        return image;
    }
}
