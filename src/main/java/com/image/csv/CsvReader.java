package com.image.csv;

import com.image.info.ImageInfo;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class CsvReader {
    public List<ImageInfo> readCSV(String csvFile) throws IOException {
        List<ImageInfo> imageInfoList = new ArrayList<>();
        Reader reader = Files.newBufferedReader(Paths.get(csvFile));
        CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT
                .withHeader("image1", "image2")
                .withFirstRecordAsHeader()
                .withIgnoreHeaderCase()
                .withTrim());
        csvParser.forEach((csvRecord) -> {
            ImageInfo imageInfo = new ImageInfo();
            imageInfo.setImage1Path(csvRecord.get("image1"));
            imageInfo.setImage2Path((csvRecord.get("image2")));
            imageInfoList.add(imageInfo);
        });
        return imageInfoList;
    }

}