package com.image.csv;

import com.image.info.ImageInfo;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class CsvWriter {
    public void writeIntoCsv(String csvFile, List<ImageInfo> imageInfoList) throws IOException {

        BufferedWriter writer = Files.newBufferedWriter(Paths.get(csvFile));

        CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT
                .withHeader("image1", "image2", "similar", "elapsed"));

        imageInfoList.forEach(imageInfo -> {
            try {
                csvPrinter.printRecord(imageInfo.getImage1Path(), imageInfo.getImage2Path(), imageInfo.getScore(), imageInfo.getElapsedTime());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        csvPrinter.flush();
    }

}