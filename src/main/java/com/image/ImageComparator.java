package com.image;

import com.image.csv.CsvReader;
import com.image.csv.CsvWriter;
import com.image.info.ImageInfo;
import com.image.process.ImageProcessor;

import java.io.IOException;
import java.util.List;

public class ImageComparator {

    private static final String CSV_INPUT = "src/main/resources/ImageDataInput.csv";
    private static final String CSV_OUTPUT= "src/main/resources/ImageDataOutput.csv";


    public static void main(String[] args) throws IOException {

        List<ImageInfo> imageInfoList = new CsvReader().readCSV(CSV_INPUT);
        imageInfoList.forEach(imageInfo -> {

            ImageProcessor processor = new ImageProcessor();
            try {
                processor.process(imageInfo);
            } catch (IOException e) {
                System.out.println("Exception while processing images : " +e.getMessage());
                e.printStackTrace();
            }
        });

        CsvWriter csvWriter = new CsvWriter();
        csvWriter.writeIntoCsv(CSV_OUTPUT, imageInfoList);
        System.out.println("Image Comparison Successful. CSV Output file being generated at : "+CSV_OUTPUT);

    }

}





